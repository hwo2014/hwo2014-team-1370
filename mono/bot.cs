using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

public class Bot
{
	public static void Main(string[] args) 
    {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) 
        {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join)
    {
		this.writer = writer;
		string line;
        string test = "";
        
        
        var MapPieces = new List<PieceInfo>();

		send(join);

		while((line = reader.ReadLine()) != null) 
        {   //zbieranie wiadomosci z serwera
            test += line;
            test += Environment.NewLine;
            test += Environment.NewLine;
            test += Environment.NewLine;
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            
			switch(msg.msgType) {
				case "carPositions":
                    //g�owy skrypt sterujacy
                    double CurrentPosition;
                    int CurrentPiece;
                    JArray pos = msg.data as JArray;
                    CurrentPosition = (double)pos[0]["piecePosition"]["inPieceDistance"]; //odczytywanie infomrmacji o pozycji
                    CurrentPiece = (int)pos[0]["piecePosition"]["pieceIndex"]; // i cz�ci toru
                    Console.WriteLine(CurrentPiece);
                    if(MapPieces[CurrentPiece].IsBend())
                    {
                        send(new Throttle(0.44));
                    }
                    else if (CurrentPiece + 1 < MapPieces.Count())
                    {
                        if (CurrentPosition > 60 && MapPieces[CurrentPiece+1].IsBend())
                        {
                            send(new Throttle(0.3));
                        }
                        else
                        {
                            send(new Throttle(1.0));
                        }
                    }                    
                    else
                    {
                        send(new Throttle(1.0));
                    }



                    //
					
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
                    JObject data = msg.data as JObject;
                    //obr�bka informacji o trasie
                    IList<string> allPieces = data["race"]["track"]["pieces"].Select(t => JsonConvert.SerializeObject(t)).ToList();
                    foreach(var eleme in allPieces)
                    {
                        PieceInfo tmp = JsonConvert.DeserializeObject<PieceInfo>(eleme);
                        MapPieces.Add(tmp);
                    }
                    //

					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
                case "yourCar":
                    break;
				default:
					send(new Ping());
					break;
			}
		}
        System.IO.File.WriteAllText(@"C:\testowanie.txt", test);
	}

	private void send(SendMsg msg)
    {
		writer.WriteLine(msg.ToJson());
	}
}

public class MsgWrapper 
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg 
{
	public string ToJson() 
    {

		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg 
{
	public string name;
	public string key;
	public string color;

	public Join(string name, string key)
    {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg 
{
	protected override string MsgType() 
    {
		return "ping";
	}
}

class Throttle: SendMsg
{
	public double value;

	public Throttle(double value)
    {
		this.value = value;
	}

	protected override Object MsgData()
    {
		return this.value;
	}

	protected override string MsgType()
    {
		return "throttle";
	}
}

class SwitchLane: SendMsg
{
    public string sSide;
    
    public SwitchLane(string sSide)
    {
        this.sSide = sSide;
    }
    protected override Object MsgData()
    {
        return this.sSide;
    }
    protected override string MsgType()
    {
        return "switchLane";
    }
}
class PieceInfo
{
    public double length;
    public double radius;
    public double angle;
    public bool switchAvaible;

    public PieceInfo(double length, double radius, double angle, bool Switch)
    {
        this.length = length;
        this.radius = radius;
        this.angle = angle;
        this.switchAvaible = Switch;
    }
    public bool IsBend()
    {
        if (this.angle != 0.0)
        {
            return true;
        }
        return false;
    }
}